resource "digitalocean_droplet" "net" {
  image = "ubuntu-20-04-x64"
  name = "net-test"
  region = "tor1"
  size = "s-1vcpu-1gb"
  //  provisioner "file" {
  //    source = "docker-compose.yml"
  //    destination = "/home/docker-compose.yml"
  //  }
  user_data = file("user_data.yml")
  ssh_keys = [digitalocean_ssh_key.net.fingerprint]
  count = 10
}