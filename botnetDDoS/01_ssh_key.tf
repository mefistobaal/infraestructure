resource "digitalocean_ssh_key" "net" {
  name = "net"
  public_key = file("../sshpubkeys/id_rsa.pub")
}