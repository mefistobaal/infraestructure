resource "digitalocean_droplet" "vpn" {
  image = "ubuntu-20-04-x64"
  name = "vpn-wireguard"
  region = "tor1"
  size = "s-1vcpu-1gb"
  //  provisioner "file" {
  //    source = "docker-compose.yml"
  //    destination = "/home/docker-compose.yml"
  //  }
  user_data = file("userdata.yml")
  ssh_keys = [digitalocean_ssh_key.vpn.fingerprint]
}