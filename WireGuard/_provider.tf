variable "digitalocean_token" {}

# Configure the provider
provider "digitalocean" {
  token = var.digitalocean_token
}