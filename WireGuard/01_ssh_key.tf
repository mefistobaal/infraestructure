resource "digitalocean_ssh_key" "vpn" {
  name = "vpn"
  public_key = file("../sshpubkeys/id_rsa.pub")
}