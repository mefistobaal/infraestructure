resource "digitalocean_record" "vpn" {
  domain = "mefistobaal.tk"
  name = "vpn"
  type = "A"
  ttl = "30"
  value = digitalocean_droplet.vpn.ipv4_address
}